#include <cstdio>
#include <iostream>
#include <cmath>
#include <map>
#include "Tester.h"


void printPassed(double quantile)
{
    printf(" => passed; quantile -- %.2f\n\n", quantile);
}


void printNotPassed(double quantile)
{
    printf(" => not passed; quantile -- %.2f\n\n", quantile);
}

void Tester::testAll()
{
    printf("\t\t\t\tAll tests\n\n");
    printf("Data size (N): %lu bits\n\n", data.size() * 8);
    int onesIn = analyzeBits(data);
    printf("Ones: \t\t\t%d\nZeroes: \t\t%lu\n\n",onesIn, (data.size() * 8) - onesIn);

    double freqTestResult = freqTest(data);
    printf("Frequency Test: %f \twith α = 0.1", freqTestResult);

    (freqTestResult >= -2.7055 && freqTestResult <= 2.7055) ? printPassed(2.7055) : printNotPassed(2.7055);

    double seqTestResult = seqTest(data);
    printf("Sequence Test: \t%f \twith α = 0.1", seqTestResult);

    (seqTestResult <= 284.3359) ? printPassed(284.3359) : printNotPassed(284.3359);

    double seriesTestResult = seriesTest(data);
    printf("Series Test: \t%f \twith α = 0.1", seriesTestResult);

    (seriesTestResult <= 37.9159) ? printPassed(37.9159) : printNotPassed(37.9159);

    double universalTestResult = universalTest(data);
    printf("Universal Test: %f\twith α = 0.01", universalTestResult);

    (universalTestResult >= -2.3263 && universalTestResult <= 2.3263) ? printPassed(2.3263) : printNotPassed(2.3263);

    autoTest(data);

}

void Tester::testAllWithOnes()
{
    data.assign(100000, 255);
    testAll();
}

int Tester::countOnes(uint8_t byte)
{
    int counter = 0;

    for (int i = 0; i < 8; i++)
    {
        if (bit(byte, i) == 1)
        {
            counter++;
        }
    }
    return counter;
}

int Tester::analyzeBits(const std::vector<uint8_t> &message)
{
    int totalNumberOfOnes = 0;

    for (unsigned char i : message)
    {
        totalNumberOfOnes += countOnes(i);
    }

    return totalNumberOfOnes;
}

double Tester::freqTest(int32_t ones, uint64_t N)
{
    double totalSize = N * 8;
    return (std::pow((totalSize - ones) - ones, 2)) / (N * 8);
}

double Tester::freqTest(const std::vector<uint8_t> &data)
{
    uint64_t N = data.size();
    int32_t ones = analyzeBits(data);
    return freqTest(ones, N);
}

double Tester::seqTest(const std::vector<uint8_t> &data)
{
    std::vector<int> freq(256);
    int L = 8;
    unsigned long N = data.size() * 8;
    for (auto byte : data)
    {
        freq[byte] += 1;
    }

    double result = 0;
    double k = N / L;

    for (int i : freq)
    {
        result += std::pow(i, 2);
    }

    result *= (256. / k);
    result -= k;

    return result;
}

double Tester::seriesTest(const std::vector<uint8_t> &data)
{
    int L = 15;
    uint64_t N = data.size() * 8;
    std::vector<uint64_t> B(static_cast<uint64_t>(L + 1));
    std::vector<uint64_t> G(static_cast<uint64_t>(L + 1));
    int lenOne = 0;
    int lenZero = 0;

    for (uint8_t byte : data)
    {
        for(int j = 0; j < 8; j++)
        {
            int bit = bit(byte, j);
            if (bit == 1)
            {
                lenOne++;
                if (lenZero <= L)
                    B[lenZero]++;
                lenZero = 0;
            } else
            {
                lenZero++;
                if (lenOne <= L)
                    G[lenOne]++;
                lenOne = 0;
            }
        }
    }
    double ones = 0;
    double zeroes = 0;

    for (int i = 1; i < L + 1; i++)
    {
        double e = (N - i + 3) / (std::pow(2, i + 2)); // estimated number of breaks/blocks
        ones += std::pow(B[i] - e, 2) / e;
        zeroes += std::pow(G[i] - e, 2) / e;
    }

    double sum = ones + zeroes;
    return  sum;
}

void Tester::autoTest(const std::vector<uint8_t> &data)
{
    printf("\n\t\t\tAUTOCORRELATION\n");
    printf("\t\tα = 0.1, quantile -- 2.71\n");
    for (int tau = 10; tau < 30; tau += 5)
    {
        int ones = 0;
        for (int i = 0; i < data.size() - tau; ++i)
        {
            for (int j = 0; j < 8; j++)
            {
                bit(data[i], j) ^ bit(data[i], j + tau) == 1 ? ones++ : 0;
            }
        }

        double res = freqTest(ones, data.size() - tau);

        printf("|Tau = %d | FreqTest = %f|",tau, res);
        (res >= -3 && res <= 3) ? printf(" => passed|\n\n") : printf(" => not passed|\n\n");
    }
}

double Tester::universalTest(const std::vector<uint8_t> &data)
{
    int L = 8;
    uint64_t N = data.size();
    double V = pow(2,L);

    auto Q = static_cast<uint32_t>(pow(2, L) * 10);
    double K;
    K = data.size() - Q;

    std::vector<int> TAB(V);
    std::vector<int> parts;

    int bit = 0;
    int tmp = 0;
    int z = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            bit = bit(data[i], j);

            tmp += bit * pow(2,z);
            z++;
            if (z == L)
            {
                parts.push_back(tmp);
                z = 0;
                tmp = 0;
            }
        }
    }

    int i = 0;
    for (i; i < Q; i++)
    {
        int b = parts[i];
        TAB[b] = i;
    }

    double sum = 0;

    for (i; i < Q + K; i++)
    {
        int b = parts[i];
        sum += log2(i - TAB[b]);
        TAB[b] = i;
    }

    sum = sum / K ;

    double e = 7.1836656;
    double d = 3.238;
    double C = 0.7 - 0.8 / L + ((4 + 32 / L) * pow(K, -(double) 3 / L)) / 15;

    double Z = (sum - e) / (C * sqrt(d));
    return Z;
}
